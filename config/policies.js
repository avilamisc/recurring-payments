
module.exports.policies = {

  BookController:{
    "*": "isAdmin",
    findOne: true,
    find: true  
  },
  UserController:{
  	"*": "isAdmin"
  }
  
};
