// RESTful routes
  // Miembros
    // GET /book/:id
    // PUT /book/:id
    // DELETE /book/:id
  // Colecciones
    // GET /book
    // POST /book
module.exports.routes = {
  '/': {
    view: 'homepage'
  },
  '/book/:id/edit': 'BookController.edit',
  '/book/:id/delete': 'BookController.delete',
  'get /signup': 'RegistrationsController.show',
  'post /signup': 'RegistrationsController.create',
  'get /login': 'AuthController.show',
  'post /login': 'AuthController.create',
  'delete /logout': 'AuthController.destroy',
  'put /admin/:id': 'UserController.admin',
  'get /subscription': 'SubscriptionController.landing',
  'get /subscription/admin': 'SubscriptionController.find',
  'get /pay/:id': 'SubscriptionController.pay',
  'post /subscription': 'SubscriptionController.create',
  'post /webhooks': 'WebhooksController.create'
};
